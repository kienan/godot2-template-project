# Godot2 Template Project

A series of folders and files that I use when starting a Godot2-based project.

The `game` folder should have the engine.cfg and any rendered assets, scenes, etc.

Use the top-level `assets` folder for storing asset sources (eg. gimp .xcf), etc. that are
required.

# Using the template

The copy the entire folder and modify as needed. The engine.cfg for the godot project
should be placed into the root of the game/ folder.

# Copyright and license

Copyright 2017 Kienan Stewart

See full copyright and license information the COPYRIGHT file.
